package lib

import "io/ioutil"

func Fr(f string) []byte {
	a, _ := ioutil.ReadFile(f)
	return a
}
