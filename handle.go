package lib

import (
	"math/rand"
	"net/http"
	"strconv"
	"strings"
)

func Handle(w http.ResponseWriter, r *http.Request) (int, string, string, []string) {
	var sess int
	sess_, _ := r.Cookie("session")
	host := r.Host
	ipad := strings.Split(r.RemoteAddr, ":")[0]
	path := strings.Split(r.URL.Path[1:], "/")
	if sess_ == nil {
		http.SetCookie(w, &http.Cookie{Name: "session", Value: strconv.Itoa(1000000000000000000 + rand.Intn(8223372036854775807)), Path: "/", MaxAge: 2147483647})
		http.Redirect(w, r, "http://"+host, http.StatusSeeOther)
	} else {
		sess, _ = strconv.Atoi(sess_.Value)
	}
	return sess, host, ipad, path
}
