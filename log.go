package lib

import (
	slog "log"
	"strings"
	"sync"
	"time"
)

var Logs Logd

type Logd struct {
	sync.Mutex
	Logx []Logx
	Loop bool
}

type Logx struct {
	Logx string
	Time time.Time
	Tran int
}

var Logp struct {
	sync.Mutex
	Logx []Logx
}

func Log(data string, tran int) int {
	data = strings.TrimRight(data, " ")
	Logs.Lock()
	if !Logs.Loop {
		Logs.Loop = true
		go func() {
			for {
				Logp.Lock()
				if len(Logp.Logx) > 0 {
					slog.Print(" ", Logp.Logx[0].Logx)
					Logp.Logx = Logp.Logx[1:]
				}
				Logp.Unlock()
				time.Sleep(time.Duration(1) * time.Second)
			}
		}()
	}
	Logs.Unlock()
	switch {
	case tran == 0:
		go func() {
			Logp.Lock()
			Logp.Logx = append(Logp.Logx, Logx{Logx: data, Time: time.Now(), Tran: 0})
			Logp.Unlock()
		}()
	case tran < -1:
		go func() {
			tran = -tran
			Logs.Lock()
			if data != Mdot {
				Logs.Logx = append(Logs.Logx, Logx{Logx: data, Time: time.Now(), Tran: tran})
			}
			Logp.Lock()
			var Logz []Logx
			for item := range Logs.Logx {
				if Logs.Logx[item].Tran == tran {
					Logp.Logx = append(Logp.Logx, Logs.Logx[item])
				} else {
					Logz = append(Logz, Logs.Logx[item])
				}
			}
			Logs.Logx = Logz
			Logp.Unlock()
			Logs.Unlock()
		}()
	case tran == -1:
		tran = int(time.Now().UnixNano())
		if data != "" {
			data = Mdot + " " + data + " " + Mdot
		}
	case tran > 0:
		go func() {
			Logs.Lock()
			Logs.Logx = append(Logs.Logx, Logx{Logx: data, Time: time.Now(), Tran: tran})
			Logs.Unlock()
		}()
	}
	return tran
}
