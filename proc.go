package lib

import "sync"

type Procs struct {
	sync.Mutex
	Procs int
}

var procs Procs

func Proc(a int) int {
	procs.Lock()
	if a != 0 {
		procs.Procs = procs.Procs + a
	}
	a = procs.Procs
	procs.Unlock()
	return a
}
