package lib

import "net/http"

func WWW(w http.ResponseWriter, r *http.Request, m string, a []byte) {
	if m != "" {
		b := HashHex(a)
		if len(r.Header["If-None-Match"]) > 0 && r.Header["If-None-Match"][0] == b {
			w.WriteHeader(304)
		} else {
			w.Header().Set("Etag", b)
			w.Header().Set("Content-Type", m)
			w.Write(a)
		}
	}
}
